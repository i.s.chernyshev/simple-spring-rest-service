package simonov.timur.restservice.web.v1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/simple")
public class SimpleController {
    @GetMapping
    public String getSimple() {
        return "Simple";
    }
}
